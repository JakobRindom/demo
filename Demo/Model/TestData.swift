//
//  TestData.swift
//  Demo
//
//  Created by Jakob Rindom on 20/03/2018.
//  Copyright © 2018 Jakob Rindom. All rights reserved.
//

import Foundation

class TestData: NSObject {
    
    class func getTestData() -> [FeedModel]  {
        var testData: [FeedModel] = []
        
        let childName = "Luna"
        let motherName = "Stine"
        let grandName = "Jytte"
        
        let data1 = FeedModel(imgUser: #imageLiteral(resourceName: "mother") , strUserName: motherName, strContent: "Idag har vi bagt kringler. Kom og smag", type: .text, imgContent: nil)
        testData.append(data1)
        
        let data2 = FeedModel(imgUser: #imageLiteral(resourceName: "mother") , strUserName: motherName, strContent: "Mmmmm vi elsker kringler", type: .image, imgContent: #imageLiteral(resourceName: "Baking"))
        testData.append(data2)
        
        
        let data3 = FeedModel(imgUser: #imageLiteral(resourceName: "grand") , strUserName: grandName, strContent: "Det ser sørme lækkert ud :)", type: .text, imgContent: nil)
        testData.append(data3)
        
        let data4 = FeedModel(imgUser: #imageLiteral(resourceName: "child") , strUserName: childName, strContent: "Elefant", type: .image, imgContent: #imageLiteral(resourceName: "dr2"))
        testData.append(data4)
        
        let data5 = FeedModel(imgUser: #imageLiteral(resourceName: "grand") , strUserName: grandName, strContent: "Den er sørme fin. Er det fra dengang vi var i zoologisk have? det må vi snart gøre igen. Det var en god tur", type: .text, imgContent: nil)
        testData.append(data5)
        
        let data6 = FeedModel(imgUser: #imageLiteral(resourceName: "child") , strUserName: childName, strContent: "Mor far og mig", type: .image, imgContent: #imageLiteral(resourceName: "dr1"))
        testData.append(data6)
        
        let data7 = FeedModel(imgUser: #imageLiteral(resourceName: "mother") , strUserName: motherName, strContent: "Vi var en tur i skoven idag. Vi var ude og se om vi kunne få øje på nogle egern", type: .image, imgContent: #imageLiteral(resourceName: "forrest"))
        testData.append(data7)
        
        return testData
        
    }
}
