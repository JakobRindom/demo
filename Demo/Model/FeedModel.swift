//
//  FeedModel.swift
//  Demo
//
//  Created by Jakob Rindom on 20/03/2018.
//  Copyright © 2018 Jakob Rindom. All rights reserved.
//

import UIKit

class FeedModel: NSObject {
    enum feedType {
        case image
        case text
    }
    
    var imgUser:UIImage
    var strUserName: String
    var strContent: String
    var type: feedType
    var imgContent: UIImage?
    
    init(imgUser:UIImage, strUserName: String, strContent: String,type: feedType, imgContent: UIImage?) {
        self.imgUser = imgUser
        self.strUserName = strUserName
        self.strContent = strContent
        self.type = type
        self.imgContent = imgContent
    }

}
