//
//  ImageDetailViewController.swift
//  Demo
//
//  Created by Jakob Rindom on 20/03/2018.
//  Copyright © 2018 Jakob Rindom. All rights reserved.
//

import UIKit

class ImageDetailViewController: UIViewController {

     //MARK: - Properties
    var selectedFeedModel: FeedModel?
    
     //MARK: - Outlets
    @IBOutlet var textViewContent: UITextView!
    @IBOutlet var imageViewContent: UIImageView!
    
    //MARK: - Life
    override func viewDidLoad() {
        super.viewDidLoad()
        if let feedModel = selectedFeedModel {
            self.textViewContent.text = feedModel.strContent
            if let image = feedModel.imgContent {
                self.imageViewContent.image = image
            } 
        }
    }
}
