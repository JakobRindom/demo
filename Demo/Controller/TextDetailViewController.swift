//
//  TextDetailViewController.swift
//  Demo
//
//  Created by Jakob Rindom on 20/03/2018.
//  Copyright © 2018 Jakob Rindom. All rights reserved.
//

import UIKit

protocol TextDetailViewControllerDelegate: class {
    func createNewFeedPost(_ feedModel: FeedModel)
}

class TextDetailViewController: UIViewController, UITextViewDelegate {
    
    //MARK: - Properties
    var selectedFeedModel: FeedModel?
    weak var delegate: TextDetailViewControllerDelegate?
    var strNewContent: String = ""
    
    //MARK: - Outlets
    @IBOutlet var textViewContent: UITextView!
    
    //MARK: - Helpers
    @objc func createNewFeedPost() {
        if let delegate = self.delegate {
            let newFeedModel = FeedModel(imgUser: #imageLiteral(resourceName: "man"), strUserName: "Ole", strContent: self.strNewContent, type: .text, imgContent: nil)
            self.navigationController?.popViewController(animated: true)
            delegate.createNewFeedPost(newFeedModel)
        }
    }
    
    //MARK: - Delegate
    func textViewDidChange(_ textView: UITextView) {
        self.strNewContent = textView.text
        if textView.text != "" {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    //MARK: - Life
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textViewContent.delegate = self
        if let feedModel = selectedFeedModel {
            self.textViewContent.text = feedModel.strContent
            self.textViewContent.isEditable = false
            self.navigationItem.rightBarButtonItem = nil
        } else {
            self.textViewContent.text = ""
            self.textViewContent.isEditable = true
            self.textViewContent.becomeFirstResponder()
            let saveButton = UIBarButtonItem(title: "Gem", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TextDetailViewController.createNewFeedPost))
            self.navigationItem.rightBarButtonItem = saveButton
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
}
