//
//  ViewController.swift
//  Demo
//
//  Created by Jakob Rindom on 20/03/2018.
//  Copyright © 2018 Jakob Rindom. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, TextDetailViewControllerDelegate {

    //MARK: - Properties
    
    let picker = UIImagePickerController()
    var selectedFeedModel: FeedModel?
    var arrModelFeed: [FeedModel] = []
    let fltCenter: CGFloat = 50
    let fltBaseline: CGFloat = -60
    
    
    //MARK:- Outlets
    @IBOutlet var collectionViewFeed: UICollectionView!
    @IBOutlet var btnCamera: UIButton!
    @IBOutlet var btnText: UIButton!
    @IBOutlet var btnAdd: UIButton!
    
    @IBOutlet var constraintBtnCameraBaseline: NSLayoutConstraint!
    @IBOutlet var constraintBtnTextBaseline: NSLayoutConstraint!
    @IBOutlet var constraintBtnCameraCenter: NSLayoutConstraint!
     @IBOutlet var constraintBtnTextCenter: NSLayoutConstraint!
    
    
    //MARK:- Actions
    @IBAction func btnCamera(_ sender: UIButton) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
         self.btnAdd(self.btnAdd)
    }
    
    @IBAction func btnText(_ sender: UIButton) {
        performSegue(withIdentifier: "textSegue", sender: self)
       self.btnAdd(self.btnAdd)
    }
    
    @IBAction func btnAdd(_ sender: UIButton) {
        if sender.currentImage == #imageLiteral(resourceName: "off") {
            self.constraintBtnTextCenter.constant = 0
            self.constraintBtnTextBaseline.constant = 0
            self.constraintBtnCameraCenter.constant = 0
            self.constraintBtnCameraBaseline.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                sender.setImage(#imageLiteral(resourceName: "on"), for: .normal)
                self.view.layoutIfNeeded()
            })
        } else {
            self.constraintBtnTextCenter.constant = fltCenter
            self.constraintBtnTextBaseline.constant = fltBaseline
            self.constraintBtnCameraCenter.constant = -fltCenter
            self.constraintBtnCameraBaseline.constant = fltBaseline
            
            UIView.animate(withDuration: 0.3, animations: {
                sender.setImage(#imageLiteral(resourceName: "off"), for: .normal)
                self.view.layoutIfNeeded()
            })
        }
    }

    //MARK: - Delegates
    
    //MARK: TextDetailViewControllerDelegate
    func createNewFeedPost(_ feedModel: FeedModel) {
       
        self.arrModelFeed.append(feedModel)
        let indexPath = IndexPath(item: self.arrModelFeed.count-1, section: 0)

        self.collectionViewFeed.insertItems(at: [indexPath])
        let bottomOffset = CGPoint(x: 0, y: self.collectionViewFeed.contentSize.height - self.collectionViewFeed.bounds.size.height+100)
        self.collectionViewFeed.setContentOffset(bottomOffset, animated: true)
        
    }
    
    //MARK: ImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let newFeedModel = FeedModel(imgUser: #imageLiteral(resourceName: "man"), strUserName: "Ole", strContent: "", type: .image, imgContent: chosenImage)
        self.arrModelFeed.append(newFeedModel)
        let indexPath = IndexPath(item: self.arrModelFeed.count-1, section: 0)
        self.collectionViewFeed.insertItems(at: [indexPath])
        let bottomOffset = CGPoint(x: 0, y: self.collectionViewFeed.contentSize.height - self.collectionViewFeed.bounds.size.height+100
        )
        self.collectionViewFeed.setContentOffset(bottomOffset, animated: true)
        
        dismiss(animated: true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrModelFeed.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellData = self.arrModelFeed[indexPath.item]
        switch cellData.type {
        case .image:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
            cell.cellData = cellData
            return cell
        case .text:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextCollectionViewCell", for: indexPath) as! TextCollectionViewCell
            cell.cellData = cellData
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.contentView.backgroundColor = UIColor.white
        cell.contentView.layer.cornerRadius = 12
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellData = self.arrModelFeed[indexPath.item]
        self.selectedFeedModel = cellData
        switch cellData.type {
        case .image:
            performSegue(withIdentifier: "imageSegue", sender: self)
        case .text:
            performSegue(withIdentifier: "textSegue", sender: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellData = self.arrModelFeed[indexPath.item]
        let width: CGFloat = self.view.bounds.width - 20
        var height: CGFloat = 0.0
        
        switch cellData.type {
        case .image:
            height = 20+8+8+30+20
            let imageWidth = width-40
            height += imageWidth * 2 / 3
        case .text:
            height = 20+8+30+20
        }
        if let font = UIFont(name:"HelveticaNeue-Light", size: 17.0) {
            height += cellData.strContent.heightWithConstrainedWidth(width-40, font: font)
        }
        return CGSize(width: width, height: height+1)
    }
    
    //MARK: Life
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionViewFeed.delegate = self
        self.collectionViewFeed.dataSource = self
        picker.delegate = self
        
        self.title = "Demo"
        
        DispatchQueue.main.async {
            self.constraintBtnTextCenter.constant = 0
            self.constraintBtnTextBaseline.constant = 0
            self.constraintBtnCameraCenter.constant = 0
            self.constraintBtnCameraBaseline.constant = 0
            self.btnCamera.layer.cornerRadius = 20
            self.btnText.layer.cornerRadius = 20
            self.btnAdd.layer.cornerRadius = 20
            self.btnAdd.clipsToBounds = true
        }
        
        self.arrModelFeed = TestData.getTestData()
    }
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "imageSegue" {
            let controller = segue.destination as! ImageDetailViewController
            if let sel = self.selectedFeedModel {
                controller.selectedFeedModel = sel
            }
            self.selectedFeedModel = nil
        }
        if segue.identifier == "textSegue" {
            let controller = segue.destination as! TextDetailViewController
            if let sel = self.selectedFeedModel {
                controller.selectedFeedModel = sel
            }
            controller.delegate = self
            self.selectedFeedModel = nil
        }
    }
}

