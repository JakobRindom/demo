//
//  TextCollectionViewCell.swift
//  Demo
//
//  Created by Jakob Rindom on 20/03/2018.
//  Copyright © 2018 Jakob Rindom. All rights reserved.
//

import UIKit

class TextCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet var lblContent: UILabel!
    @IBOutlet var lblUser: UILabel!
    @IBOutlet var imageUser: UIImageView!
    
    //MARK: - Properties
    var cellData: FeedModel? {
        didSet {
            if let data = cellData {
                self.imageUser.image = data.imgUser
                self.lblUser.text = data.strUserName
                self.lblContent.text = data.strContent
            }
        }
    }
    
    //MARK: - Live
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.imageUser.layer.cornerRadius = 15
            self.imageUser.layer.borderWidth = 0.1
            self.imageUser.layer.borderColor = UIColor.lightGray.cgColor
            self.imageUser.layer.masksToBounds = true
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageUser.image = nil
        self.lblContent.text = ""
        self.cellData = nil
        self.lblUser.text = ""
    }
    
}
